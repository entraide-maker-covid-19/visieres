# Montage visière [EMC19]Blackbelt_remix

- ![visière blackbelt_remix](images/01_montageEMC19.jpg)
  - Lien d'origine : discord "Entraide maker - COVID 19"
  - Modèle inspiré par : https://www.linkedin.com/pulse/3d-printed-covid-19-face-shield-mask-manual-makers-schürmann/ 
             ainsi que : https://www.prusaprinters.org/prints/25857-prusa-protective-face-shield-rc2

## Le matériel

* Pièce imprimée:
- ![Pièces imprimée](https://gitlab.com/entraide-maker-covid-19/visieres/-/raw/patch-1/images/20200326_151922.jpg)
* Visière (Feuille PVC A4):
- ![Feuille pvc](https://media.maxiburo.fr/Images_Basse_Definition/Zoom/66/96/66963.jpg)
* Elastiques:
- ![Elastiques Classiques](https://www.toutembal.fr/client/gfx/photos/produit/bracelet_elastique_en_caoutchouc_1mm8_1155.jpg)
* Ruban élastique (environ 15mm de large)
- ![Ruban Elastiques](https://www.moench-elastic.de/fileadmin/template/bilder/30_material_streifen_web.jpg)
* Perforeuse A4 (2 ou 4 trous)
- ![Perforeuse A4 (2 ou 4 trous)](https://www.malinelle.com/ressources/cache/produits/softcarrier/bind/6/1/61165-302_336x340.jpg)

## Le Montage

* Percer la feuille avec la perforeuse pour feuilles A4

* Placer la feuille en paysage par rapport au support de visière et insérer un des trous à l'extrémité de la feuille dans un des tétons sur le coté:
* ![Montage Feuille 1/2](images/02_montageEMC19.jpg)

*  Insérer le trou de l'autre extrémité de la feuille sur le téton du même côté.
* ![Montage Feuillle 2/2](images/03_montageEMC19.jpg)

* Montage des élastiques par les crochets sur le côté ou de la bande élastique par les passants intégrés aux branches :
* ![Montage Ruban Elastique](images/04_montageEMC19.jpg)
 
 :warning: Le port d'une visière anti-projections ne dispense pas du respect des gestes barrières :warning:
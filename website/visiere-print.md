---
layout: page
title: Visière de protection par l'impression 3D
permalink: /visiere/impression-3D
background: 
nav: 
---

## info

Page en cours de rédaction, le contenu peux changer à tout moment.

## avant propos
Les visières sont un des élèments de protection qui peuvent être utiliser en milieu hospitalier.

## les modèles 

<div class="autogrid has-gutter txt-center">

<div>
#### EMC19 blackbelt remix
<span class="thumb thumb-h200">![](https://gitlab.com/entraide-maker-covid-19/visieres/-/raw/master/images/xavmix.jpg)</span>   
[Télécharger le modèle](https://gitlab.com/entraide-maker-covid-19/visieres/-/raw/master/Visieres_impression_3D/backbelt_remix_xav/Visi%C3%A8re%20EMC19%20-%20blackbelt%20remix/EMC19_blackbelt_remix.stl?inline=false)  
[Télécharger le modèle slack x2](https://gitlab.com/entraide-maker-covid-19/visieres/-/raw/master/Visieres_impression_3D/backbelt_remix_xav/Visi%C3%A8re%20EMC19%20-%20blackbelt%20remix/EMC19_blackbelt_remix_STACKx2.stl?inline=false)    
[Télécharger le modèle slack x5](https://gitlab.com/entraide-maker-covid-19/visieres/-/raw/master/Visieres_impression_3D/backbelt_remix_xav/Visi%C3%A8re%20EMC19%20-%20blackbelt%20remix/EMC19_blackbelt_remix_STACKx5.stl?inline=false)         
[Télécharger le modèle slack x10](https://gitlab.com/entraide-maker-covid-19/visieres/-/raw/master/Visieres_impression_3D/backbelt_remix_xav/Visi%C3%A8re%20EMC19%20-%20blackbelt%20remix/EMC19_blackbelt_remix_STACKx10.stl?inline=false)        

[Télecharger la documentation](https://gitlab.com/entraide-maker-covid-19/visieres/-/raw/master/Visieres_impression_3D/backbelt_remix_xav/Notice%20de%20montageEMC19.pdf?inline=false)
</div>

<div>
#### EMC19 Version légère
<span class="thumb thumb-h200">![](https://gitlab.com/entraide-maker-covid-19/visieres/-/raw/master/images/xavmix.jpg)</span>   
[Télécharger le modèle](https://gitlab.com/entraide-maker-covid-19/visieres/-/raw/master/Visieres_impression_3D/backbelt_remix_xav/Visi%C3%A8re%20EMC19%20-%20Version%20l%C3%A9g%C3%A8re/EMC19_light-edition.stl?inline=false)  
[Télécharger le modèle slack x5](https://gitlab.com/entraide-maker-covid-19/visieres/-/raw/master/Visieres_impression_3D/backbelt_remix_xav/Visi%C3%A8re%20EMC19%20-%20Version%20l%C3%A9g%C3%A8re/EMC19_light-edition_stackX5.stl?inline=false)    
[Télécharger le modèle slack x10](https://gitlab.com/entraide-maker-covid-19/visieres/-/raw/master/Visieres_impression_3D/backbelt_remix_xav/Visi%C3%A8re%20EMC19%20-%20Version%20l%C3%A9g%C3%A8re/EMC19_light-edition_stackX10.stl?inline=false)        

[Télecharger la documentation](https://gitlab.com/entraide-maker-covid-19/visieres/-/raw/master/Visieres_impression_3D/backbelt_remix_xav/Notice%20de%20montageEMC19.pdf?inline=false) 
</div>

<div>
#### Visière solidaire
<span class="thumb thumb-h200">![](https://visieresolidaire.org/css/logo-officiel-vs.png)</span>  
Le modèle de visière solidaire évolue régulièrement, veuillez vous réfèrer au site internet et au groupe facebook pour avoir leur dernière version.    
Lien de téléchargement en bas de page [visieresolidaire.org](https://visieresolidaire.org/fr/)
</div>

</div>



### Liens utiles
 - [fabricommuns.org](https://fabricommuns.org/)
 - [visieresolidaire.org](https://visieresolidaire.org/fr/)
 - [covid3d.fr](https://www.covid3d.fr/)

---
layout: page
title: Visière de protection
permalink: /visiere/
background: 
nav: true
---

# Visière de protection

## info

Page en cours de rédaction, le contenu peux changer à tout moment.

## avant propos
Les visières sont un des élèments de protection qui peuvent être utiliser en milieu hospitalier.

## les modèles

<div class="autogrid has-gutter txt-center">

<div>
#### Impression 3D
<span class="thumb thumb-h200">![](https://gitlab.com/entraide-maker-covid-19/visieres/-/raw/master/images/xavmix.jpg)</span>   
[Voir les modèles imprimable](/visiere/impression-3D)
</div>

<div>
#### Découpe laser
[Voir les modèles imprimable](/visiere/decoupe-laser)
</div>


</div>


### Liens utiles
 - [fabricommuns.org](https://fabricommuns.org/)
 - [visieresolidaire.org](https://visieresolidaire.org/fr/)
 - [covid3d.fr](https://www.covid3d.fr/)

## Procédure de désinfection

Voir dans le dossier "Procédures"

 Nous avons pour l'instant :

* Désinfection du PLA

## Liens vers des fournisseurs de feuilles plastiques

* Bureau vallée (achat possibla des "drives")
* Eventuellement Office dépot
* Amazon (selon politique des livraisons en cours)
* N'Hésitez pas à contacter des fournisseurs locaux afin d'être prioritaires (si vous avez une demande d'un hôpital, ce dernier peut appuyer votre commande)


## Vous êtes dans un fablab ? 

 - Comment produire et distribuer les visières en sécurité ?     
    Voir [le guide du Réseau Français des Fablabs](http://www.fablab.fr/coronavirus/prototypage-et-projets/article/2-visieres-de-protection-coronavirus)
 - un guide pour accéder au lab en période de confinement   
    [sur le site du Réseau Français des Fablabs](http://www.fablab.fr/coronavirus/prototypage-et-projets/article/acces-aux-fablabs-en-periode-de-confinement)

## Autres

* Logiciel pour organiser automatiquement de nombreux tracés sur une plaque (pour découpe laser ou CNC) : https://deepnest.io/

**Si vous êtes symptomatique ( de la fièvre ou une sensation de  fièvre, de la toux, des difficultés respiratoires ) ne prenez pas le  risque de contaminer d’autres personnes, restez chez vous n’interagissez pas avec d’autres.**

# Visieres 2

**Voici la liste des visières sélectionnées par le groupe de travail sur le Discord.**

- [x] Modèles 3D rapide
- [x] Modèle 3D tres rapide
- [x] Modèles laser
- [x] Modèle laser PP ou PET
- [ ] Notice de montage
- [x] Notice de désinfection (en cours)
- [ ] Modèle de décharge à faire signer

Une liste à voir ici : https://fablab-ulb.gitlab.io/projects/coronavirus/protective-face-shields/

## Modèles sélectionnés pour l'impression 3D

Nous avons sélectionné deux modèles de visières :

Les critères de sélections sont :

* Temps d'impression
* Solidité
* Confort
* Nettoyable ou pas (facilement démontable)

### Blackbelt_remix2 par Xav

Modèle du discord utilisant une perforeuse de classeur pour faire les trous dans le plastique. La distance entre les trous est celle d'une troueuse A4.

Existe en version light (En cours de validation)

- [ ] Clips
- [x] Trous perforeuse
- [x] Stackable
  (x10 et x5 pour la version light)
- [x] Elastique
- [x] <1h (version light)

![Modèle classique](images\20200326_151922.jpg)

![version light]( images/xavmix.jpg)




* Temps impression estimé : 1h07 (40min version light)
* Vitesse : 60mm/s
* Couches : 0,28mm
* Buse de 0,4mm
* remplissage 0% (2 perimètres)
* Concepteur : Xav
* Lien d'origine : discord entraide maker - COVID 19
* Modèle dérivé de : https://www.linkedin.com/pulse/3d-printed-covid-19-face-shield-mask-manual-makers-sch%C3%BCrmann/

### 3DVFaceShield

- [ ] Clips
- [x] Trous perforeuse
- [x] Stackable
- [x] Elastique 
  (au choix avec ou sans)
- [x] <1h

Un des modèles les plus répandus car très rapide à imprimer (20min), très léger  et avec ou sans élastique. Existe en version Américaine (6 trous) et Européenne (4 trous)

![3DV](images\3DV.jpg)

* Site des concepteurs (version originale, photo du dessus) : https://3dverkstan.se/protective-visor/
* Versions US et UE (6 et 4 trous) : https://retrolabs.com/3d-prints/3d-printed-face-shield-with-10-mil-pvc cliquez sur les images pour télécharger les modèles.

### Visière solidaire - Covid19 

* Le ruban frontal est assez large pour être agréable à porter. 
* Les modèles trop fins ont tendance à ne pas tenir ou à être désagréable.
* L'utilisation des clips permet de la démonter facilement afin de la nettoyer.

L'utilisation des clips permet de la démonter facilement afin de la nettoyer.

![visière solidaire](https://gitlab.com/entraide-maker-covid-19/visieres/-/raw/master/images/visieresolidaire.jpg)

- Visière du groupe facebook : utilisation de clips imprimés à part.

  - Temps impression : 1h
  - Vitesse : 100mm/s
  - Couches : 0,3mm 
  - Concepteur : Yann Vodable
  - Lien d'origine : https://www.thingiverse.com/thing:4238879
  - Groupe Facebook : https://www.facebook.com/groups/2454266324886624/

## Modèles sélectionnés pour la découpe laser

Cette liste est tirée d'un grand nombre d'essais sur plusieurs dizaines de modèles différents afin d'en dégager les meilleurs en terme de vitesse de fabrication, de simplicité de montage, de solidité et surtout le confort de l'utilisateur

### Le choix du modèle ce fait en fonction du matériau et de l'épaisseur dont vous disposez :

| Flexibilité | Epaisseur | Matières Validés | choix              | Fichier                                                      | Auteur                         |
| ----------- | --------- | ---------------- | ------------------ | ------------------------------------------------------------ | ------------------------------ |
| Faible      | 2 à 6mm*  | PMMA - PETG -    | Moustache épaisse  | Sur ce dépôt                                                 | Les Ateliers De Hippo          |
| Faible      | 7 à 9mm   | PMMA - PETG -    | Moustache fine     | Sur ce dépôt                                                 | Les Ateliers De Hippo          |
| Moyene      | 1 à 2mm   | PETG -           | Proto Shield       | [protoheaven.org](https://www.protohaven.org/proto-shield/) Et sur ce dépôt | Mat Thorne et Devin Montgomery |
| Faible      | 3mm       | PMMA -           | Masque LaserSystem | [cult3d.com](https://cults3d.com/fr/mod%C3%A8le-3d/outil/masque-visiere-anti-projection-pour-laser) | LaserSystem                    |
| Forte       | ?mm       | Delrin/POM       | Bandeau Delrin     | [Univ. SMIOT](http://smiot.univ-tln.fr/index.php/codiv-19/)  | SMIOT                          |

* Privilégiez la version de LaserSystem si vous avez du PMMA de exactement 3mm d'épaisseur!

### Modèle "moustache" par Hippolyte

Modèle du Discord. Production en cours dans certains labs (plusieurs centaines de pièces).

Cette visière a pour but de maximiser la quantité de masque sur une même surface de matière et quel que soit son épaisseur.
Elle à été conçue de base pour du PMMA mais devrais rester utilisable pour d'autres matières qui peuvent ce tordre a chaud.

* Très peu gourmant en matière
* Rapide à fabriquer
* Parties en contact avec la peau clipsables imprimées en 3D pour plus de confort
* En attente de retours du personnel médical
* Instructions de montage dans le README du dossier "modèle moustache"

![DSC_0355](https://gitlab.com/entraide-maker-covid-19/visieres/-/raw/master/images/moustache/DSC_0355.JPG)

### Modèle "Folded Face Shield"

Extrêmement rapide à monter, à découpe dans du PP ou PET

https://gitlab.com/volumes1/visi-re-plexi-volumes

### Modèle d'Electrolab (plastique souple)

Modèle à base de plastique souple 

![electrolab2](images\electrolab2.png)

https://code.electrolab.fr/covid-19/visiere

### Modèle Proto Shield

Conçu par Mat Thorne et Devin Montgomery, ce modèle est compatible avec les visière imprimées Prusa.

![]( images/lasersystem.jpg)

* Lien d'origine : https://www.protohaven.org/proto-shield/

### Modèle de YouFactory 

De Lyon, validé par les hôpitaux du coin, peu gourmand en matière, pas d'élastique (donc facile à nettoyer) : 

* Lien : https://grabcad.com/library/covisiere-youfactory-v1-1

### Un autre modèle à la découpeuse laser

* https://cults3d.com/fr/mod%C3%A8le-3d/outil/masque-visiere-anti-projection-pour-laser

* Version en MDF (jetable) : https://www.cdamlab.com/faceshield.html

## Procédure de désinfection

Voir dans le dossier "Procédures"

 Nous avons pour l'instant :

* Désinfection du PLA

## Liens vers des fournisseurs de feuilles plastiques

* Bureau vallée ont des drives.
* Eventuellement Office dépot
* Amazon
* Sur Paris : si vous avez une capacité de production (plusieurs imprimantes, une découpeuse laser) une grosse réserve de feuilles va être donnée, voir avec @blaise sur le Discord.
* Hésitez pas à contacter des fournisseurs locaux afin d'être prioritaires (si vous avez une demande d'un hôpital, ce dernier peut appuyer votre commande).
## Vous êtes dans un fablab ? 

* Comment produire et distribuer les visières en sécurité ? Voir le guide du Réseau Français des Fablabs : http://www.fablab.fr/coronavirus/prototypage-et-projets/article/2-visieres-de-protection-coronavirus
* Voici un guide pour accéder au lab en période de confinement : http://www.fablab.fr/coronavirus/prototypage-et-projets/article/acces-aux-fablabs-en-periode-de-confinement

## Autres

* Logiciel pour organiser automatiquement de nombreux tracés sur une plaque (pour découpe laser ou CNC) : https://deepnest.io/
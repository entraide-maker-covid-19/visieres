# La visière Moustache

Cette visière a pour but de maximiser la quantité de masque sur une même surface de matière et quel que soit son épaisseur.
Elle à été designé de base pour du PMMA mais devrais rester utilisable pour d'autres matières qui peuvent ce tordre a chaud.

La version actuelle : V03 à été testé par differents fablab.
Nous somme dans l'attente de retours par du personnel médical.

Le nom moustache viens de sa forme lorsqu'elle est positionné sur le plateau de découpe.

## La découpe

Je conseille de laisser un espace de 1mm au minimum entre chaque occurence.
Si vraiment vous êtes confiant sur votre machine vous pouvez passer sur du 0.5mm.

Si vous avez la possibilité et si vous y arrivez, je vous conseille égualement de déscendre le point de focal au milieux de l'épaisseur à découper afin de créer des chanfreins naturel par le double cone de focalisation.

## Le pliage

Le pliage des branches ce fait à chaud pour la pluspart des matières.
Deux techniques s'offrent à vous en fonction du matos que vous avez.

### Technique du pistolet à air chaud

Faire chauffer le materiau sur une surface non adhérante.
Ne pas crammer la surface avec une chaleur trop importante.
Priviligier une montée en température progressive afin de chauffer aussi l'interieur du materiaux et pas uniquement l'exterieur.

### Technique de la poêle

Faite bouillir de l'eau dan une poêle.
Tremper la partie à plier verticalement dans l'eau.
Attendre 20seconde avant d'exercer une pression progressive jusqu'a plier la branche dans la bonne position.
Une fois en position attendre que la pression ne ce fasse plus ressentir.
Sortir de l'eau. Servir froid.

## Mise en place de la feuille.

L'écartement au centre respecte l'écartement standard de 80mm.
Sur les côtés c'est plus complèxe.

En fonction de la trotion appliqué vous avez une distance plus ou moins importante de percement à effectuer.

## Optionnel : Impression et mise en place des pads de protection
/!\ En dessous de 7mm les pads de semble personellement indispenssable /!\
La technique pour palier a ça reste l'utilisation de bandeau de tissue a l'emplacement du support de visière.

Pour les imprimer, pas de raft ou autre technique d'adhésion.
Pas de support.
Privilégiez une bonne finition à une vitesse trop rapide.

Les pads ce placent à chaud pour épouser la forme de la visière.
Vous avez une version par pas de 1mm pour des epaisseurs de 2mm jusqu'a 8mm

Bonne découpe et impression !
